#!/bin/bash
# GiTeX: a high-yet-still-pretty-low-level layer for document preparation and management
# 
# Copyright 2016 jeremy theler <jeremy@seamplex.com>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

# check that the environment is sane
for i in git m4 date; do
  if [ -z `which $i` ]; then
    echo "error: $i not installed"
    exit 1
  fi
done

if [ -z `which xelatex` ]; then
  echo "error: gitex depends on xelatex (not just latex) which is not installed"
  exit 1
fi


function callxelatex {
  count=1
  while [[ ! -e latex.out ]] || [[ ! -z "`grep -i rerun latex.out | grep -v sty | grep -v biblatex`" ]]; do
    echo -n "calling xelatex $1 ... "
    xelatex -interaction=nonstopmode -halt-on-error $1.tex > latex.out
    if [ $? = 0 ]; then
      echo "ok!"
    else
      echo "failed!"
      echo "----8<---- tail of latex.out begins ----8<----"
      tail -n 20 latex.out
      echo "----8<----------------8<----------------8<----"

      echo "offending command was"
      echo "xelatex $1.tex"
      exit 1
    fi
    count=`expr ${count} + 1`
  done
}

function callbiber {
  if [ ! -z "`grep Biber latex.out`" ]; then
    if [ -z `which biber` ]; then
      echo "error: biber not installed (and needed)"
      exit 1
    fi
    echo -n "calling biber $1 ... "
    biber $1 > biber.out
    if [ $? = 0 ]; then
      echo "ok!"
      rm -f biber.out latex.out
    else
      echo "failed! (see biber.out for details)"
      exit 1
    fi
    callxelatex $1
  fi
}


# create some useful variables
currentauthor=`echo gitex_firstauthor | m4 fields.m4 - | grep -v gitex`
currentemail=`echo gitex_firstauthoremail | m4 fields.m4 - | grep -v gitex`

if [ -z "${currentauthor}" ]; then
  currentauthor=`git config user.name`
fi
if [ -z "$currentauthor" ]; then
  currentauthor=`whoami`
fi

if [ -z "${currentemail}" ]; then
  currentemail=`git config user.email`
fi

# TODO: find a better way to do this
# TODO: complain if locale is not installed
language=`echo gitex_mainlanguage | m4 fields.m4 -`
if [ "x${language}" == "xenglish" ]; then
  DATELC=`locale -a | grep en | head -n1`
elif [ "x${language}" == "xspanish" ]; then
  DATELC=`locale -a | grep es | head -n1`
fi
currentdate=`LC_ALL=${DATELC} date +%_d-%b-%Y`
currentdateedtf=`LC_ALL=${DATELC} date +%Y-%m-%d`

author=`echo gitex_firstauthor | m4 fields.m4 - | grep -v gitex`
email=`echo gitex_firstauthoremail | m4 fields.m4 - | grep -v gitex`

if [ -z "${author}" ]; then
  author=`git log -1 --format="%an"`
fi
if [ -z "${authoremail}" ]; then
  authoremail=`git log -1 --format="%ae"`
fi

branch=`git rev-parse --abbrev-ref HEAD`
dochash=`git rev-parse --short HEAD`

if [ -e "baserev" ]; then
  basehash=`cat baserev`
else
  basehash=`git rev-parse HEAD`
fi


rm -rf revhistory.tex
headepoch=`git log -1 --format="%ct"`
headdate=`LC_ALL=${DATELC} date -d@${headepoch} +%_d-%b-%Y`
headdateedtf=`LC_ALL=${DATELC} date -d@${headepoch} +%Y-%m-%d`


tag=`git tag --sort=-version:refname | head -n1`

if [ -z "${tag}" ]; then
  revision="DRAFT"
  docver=`git rev-list ${basehash}..HEAD | wc -l`
  echo "DRAFT & ${currentdate} & ${currentauthor} & Draft previous to the first issue \\\\" > revhistory.tex
else
  revision=${tag}
  docver=`git rev-list ${tag}..HEAD | wc -l`
fi

if [ ${docver} -ne 0 ]; then
  revision="${revision}${docver}"
fi   

# if the current working tree is not clean, we add a "+"
# (should be $\epsilon$, but it would screw the file name),
# set the date to today and change the author to the current user,
# as it is the most probable scenario
if [ -z "`git status --porcelain`" ]; then
  date=${headdate}
  dateedtf=${headdateedtf}
else
  revision="${revision}+"
  date=${currentdate}
  dateedtf=${currentdateedtf}
  author=${currentauthor}
  authoremail=${currentemail}
fi

for histtag in `git tag --sort=-version:refname`; do
  histauthor=`git log -1 ${histtag} --format="%an"`
  histeepoch=`git log -1 ${histtag} --format="%ct"`
  histdate=`LC_ALL=${DATELC} date -d@${histeepoch} +%_d-%b-%Y`
  histdesc=`git tag -ln ${histtag} | cut -c6-`

  echo "${histtag} &  ${histdate} &  ${histauthor} &  ${histdesc} \\\\" >> revhistory.tex
done


# cover
if [ -e "cover-back.m4.svg" ]; then
  m4 \
    -Dgitex_date="${date}" \
    -Dgitex_author="${author}" \
    -Dgitex_authoremail="${authoremail}" \
    -Dgitex_revision="${revision}" \
    -Dgitex_dochash="${dochash}" \
    fields.m4 cover-back.m4.svg > cover-back.svg || exit 1

  ./svg2pdf.sh cover-back.svg || exit 1

elif [[ ! -e "cover-back.pdf" ]] || [[ `stat -c %Y cover-back.svg` -gt `stat -c %Y cover-back.pdf` ]]; then
  ./svg2pdf.sh cover-back.svg || exit 1
fi


# document
docnumber=`echo gitex_docnumber | m4 fields.m4 - | tr ' ' '_'`
m4 \
   -Dgitex_date="${date}" \
   -Dgitex_author="${author}" \
   -Dgitex_authoremail="${authoremail}" \
   -Dgitex_revision="${revision}" \
   -Dgitex_dochash="${dochash}" \
   fields.m4 document.m4.tex  > ${docnumber}-${revision}.tex || exit 1

if [ -e hook-pre.sh ]; then
 . hook-pre.sh ${docnumber}-${revision}.tex
fi


# main call
rm -f latex.out
rm -f current.tex
ln -s ${docnumber}-${revision}.tex current.tex
callxelatex ${docnumber}-${revision}
callbiber ${docnumber}-${revision}
callxelatex ${docnumber}-${revision}
rm -f current.pdf
ln -s ${docnumber}-${revision}.pdf current.pdf
pages=`grep "Output written" latex.out  | cut -d'(' -f2  | cut -d' ' -f1`
rm -f latex.out

# agregamos lo que generamos .gitignore para que no nos moleste
if [ -z "`grep ${docnumber} .gitignore`" ]; then
  echo ${docnumber}\* >> .gitignore
fi

# y hacemos un poco de info en json
title=`echo gitex_title | m4 fields.m4 - | grep -v gitex`
cat << EOF > gitex.json
{
  "title": "${title}",
  "author": "${author}",
  "number": "${docnumber}",
  "date": "${dateedtf}",
  "hash": "${dochash}",
  "filename": "${docnumber}-${revision}.pdf",
  "revision": "${revision}",
  "pages": ${pages}
}
EOF

# y el bib
key=`echo gitex_title | m4 fields.m4 - | tr -d ' '`
type=`echo gitex_doctype | m4 fields.m4 - | grep -v gitex`
institution=`echo gitex_institution | m4 fields.m4 - | grep -v gitex`

cat << EOF > gitex.bib
@techreport{${key},
 title = "${title}",
 author = "${author}",
 number = "${docnumber}",
 version = "${revision}",
 date = "${dateedtf}",
 language = "${language}",
 hyphenation = "${language}", 
EOF

if [ ! -z "${type}" ]; then
  echo " type = \"${type}\"," >> gitex.bib
fi

if [ ! -z "${institution}" ]; then
  echo " institution = \"${institution}\"," >> gitex.bib
fi

cat << EOF >> gitex.bib
}
EOF
